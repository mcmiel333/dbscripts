Create or replace package mc_external as


FUNCTION getExternalData Return VARCHAR2;
--------------------------------------------
--
--Add Parameter to makeLclFiscal FUNCTION
--
--------------------------------------------
FUNCTION makeLclFiscal(startYear, endYear) Return VARCHAR2;

End;

--------------------------------------------
--
--Add Parameter to getExternalData FUNCTION
--
--------------------------------------------


FUNCTION getExternalData as VARCHAR2

output:= makeLclFiscal(getMaxDate);

return output

End;


FUNCTION makeLclFiscal(startYear, endYear) as VARCHAR2
	
	
	endYear = startYear + 1;
	
	Select * from mc_lcl_fiscal
	where fiscal_yr_num >= startYear and fiscal_yr_num <= endYear;
	
	return 'Ok';

End;


--------------------------------------------
--
--Add Parameter to getMaxDate FUNCTION
--
--------------------------------------------

FUNCTION getMaxDate AS NUMBER

	Select max(fiscal_yr_num)
	from Fiscal_Calendar;
	
	return getMaxDate;

End;